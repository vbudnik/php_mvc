<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 10/30/18
 * Time: 9:52 PM
 */

function __autoload($class_name){
    $array_paths = array(
        '/models/',
        '/components/'
    );

    foreach ($array_paths as $path) {
        $path = ROOT. $path. $class_name . '.php';
        if(is_file($path)){
            include_once $path;
        }
    }
}