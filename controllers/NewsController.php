<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 10/25/18
 * Time: 7:47 PM
 */

include_once ROOT.'/models/News.php';

class NewsController
{

    public function actionIndex(){
        $newsList = array();
        $newsList = News::getNewsList();

        require_once(ROOT. '/views/news/category.php');

        return true;
    }

    public function actionView($id)
    {
        if ($id) {
            $newsItem = News::getNewsItemById($id);
            echo '<pre>';
            print_r($newsItem);
            echo '</pre>';
        }
        return true;
    }
}