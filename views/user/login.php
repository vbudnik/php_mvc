<?php include ROOT. '/views/layout/header.php';?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-4 padding-right">

                <?php if(isset($errors) && is_array($errors)): ?>
                    <ul>
                        <?php foreach ($errors as $error):?>
                            <li>- <?php echo $error; ?></li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>

                <div class="signup-form">
                    <h2>Login in site</h2>
                    <form action="#" method="post">
                        <input type="email" name="email" placeholder="E-mail" value="email"/>
                        <input type="password" name="password" placeholder="Pass" value="password"/>
                        <input type="submit" name="submit" class="btn btn-default" value="submit"/>
                    </form>
                </div>

                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT. '/views/layout/footer.php';?>
